const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const todoSchema = require('../schemas/todoSchema');
const checkLogin = require('../middlewires/checkLogin');

//create model with schema
const Todo = new mongoose.model('Todo', todoSchema);

//get all the todos
router.get('/', checkLogin, (req, res) => {
	//find all the column
	//  Todo.find(req.body, (err, getResp)=>{
	// 	if (err) {
	// 		res.status(500).json({
	// 			qstatus: 'unsuccessful',
	// 			error: err.message
	// 		});
	// 	} else {
	// 		res.status(200).json({
	// 			qstatus: 'successful',
	// 			rows: getResp,
	// 			length: getResp.length
	// 		});
	// 	}
	// });

	//find only selected column
	Todo.find(req.body).select({
		__v: 0 //without __v
	}).exec((err, getResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			res.status(200).json({
				qstatus: 'successful',
				rows: getResp,
				length: getResp.length
			});
		}
	});
});

// get active todos with custom function using async/await
router.get('/active', async (req, res) => {
	const todo = new Todo();
	try {
		const result = await todo.activeTodos();
		res.status(200).json({
			qstatus: 'successful',
			rows: result,
			length: result.length
		});
	} catch (err) {
		res.status(500).json({
			qstatus: 'unsuccessful',
			error: err.message
		});
	}
});

//get active todos with custom function using callback
router.get('/active-callback', (req, res) => {
	const todo = new Todo();
	todo.activeTodosWithCallback((err, acResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			res.status(200).json({
				qstatus: 'successful',
				rows: acResp,
				length: acResp.length
			});
		}
	});
});

//get active todos with static method
router.get('/js', (req, res) => {
	Todo.findByJS((err, acResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			res.status(200).json({
				qstatus: 'successful',
				rows: acResp,
				length: acResp.length
			});
		}
	});
});

//get active todos with query-helper
router.get('/language', (req, res) => {
	const lan = req.query.lan || '';
	Todo.find().byLanguage(lan, (err, acResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			res.status(200).json({
				qstatus: 'successful',
				rows: acResp,
				length: acResp.length
			});
		}
	});
});

//get a todo
router.get('/:id', (req, res) => {
	Todo.find({ _id: req.params.id }, (err, getResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			res.status(200).json({
				qstatus: 'successful',
				rows: [getResp],
				length: [getResp].length
			});
		}
	});
});

//post a todo
router.post('/', (req, res) => {
	const newTodo = new Todo(req.body);
	newTodo.save((err, insertionResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			const rows = [insertionResp];
			res.status(200).json({
				qstatus: 'successful',
				rows: rows,
				length: rows.length
			});
		}
	});
});

//post multiple todo
router.post('/all', (req, res) => {
	Todo.insertMany(req.body, (err, insertionResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			const rows = insertionResp;
			res.status(200).json({
				qstatus: 'successful',
				rows: rows,
				length: rows.length
			});
		}
	});
});

//put/update a todo
router.put('/:id', (req, res) => {
	//updateOne is used for updateing one row
	//updateMany is used for updating array of rows
	//if we need to get updated rows as resp, used findbyidandupdate
	//  Todo.updateOne({ _id: req.params.id }, { $set: req.body }, (err, upResp) => {
	Todo.findByIdAndUpdate(
		{ _id: req.params.id },
		{ $set: req.body },
		{ useFindAndModify: false, new: true },
		(err, upResp) => {
			if (err) {
				res.status(500).json({
					qstatus: 'unsuccessful',
					error: err.message
				});
			} else {
				res.status(200).json({
					qstatus: 'successful',
					rows: [upResp],
					length: [upResp].length
				});
			}
		});
});

//delete todo
router.delete('/:id', (req, res) => {
	Todo.deleteOne({ _id: req.params.id }, (err, getResp) => {
		if (err) {
			res.status(500).json({
				qstatus: 'unsuccessful',
				error: err.message
			});
		} else {
			res.status(200).json({
				qstatus: 'successful',
				rows: [getResp],
				length: [getResp].length
			});
		}
	});
});

module.exports = router;
