const jwt = require('jsonwebtoken');

const checkLogin = (req, res, next) => {

	try {
		const { authorization } = req.headers;
		const token = authorization.split(' ')[1];
		const decoded = jwt.verify(token, process.env.JWT_SECRET);
		const { user, userId } = decoded;
		req.username = user;
		req.userId = userId;
		next();
	} catch {
		next('Authentication failed')
	}

}

module.exports = checkLogin;
