const mongoose = require('mongoose');

//schema
const todoSchema = mongoose.Schema({
	title: {
		type: 'String',
		required: true
	},
	description: {
		type: 'String',
		required: false
	},
	status: {
		type: 'String',
		enum: ['active', 'inactive']
	},
	date: {
		type: 'date',
		default: Date.now()
	}
});

//for instance methods
todoSchema.methods = {
	activeTodos: function () {
		return mongoose.model('Todo').find({ status: 'active' });
	},
	activeTodosWithCallback: function (cb) {
		return mongoose.model('Todo').find({ status: 'active' }, cb);
	}
}

//static methods
todoSchema.statics = {
	findByJS: function (cb) {
		return this.find({ title: /js/i }, cb);
	}
}

//query helper
todoSchema.query = {
	byLanguage: function (language, cb) {
		return this.find({ title: new RegExp(language, 'i') }, cb);
	}
}

// https://www.youtube.com/watch?v=BtCGiEHzdfk&t=3062s

module.exports = todoSchema;