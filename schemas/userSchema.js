const mongoose = require('mongoose');

//schema
const userSchema = mongoose.Schema({
	name: {
		type: 'String',
		required: true
	},
	username: {
		type: 'String',
		required: true
	},
    password: {
		type: 'String',
		required: true
	},
	status: {
		type: Boolean,
		default: true
	}
});

module.exports = userSchema;