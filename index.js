const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const todoHandler = require('./routerHandler/todoHandler');
const userHandler = require('./routerHandler/userHandler');

//app object
const app = express();
//accessing .env file
dotenv.config();
//port number
const PORT = 3000;

//middleware
app.use(express.json());

//database connection with mongoose
mongoose.connect('mongodb://localhost/todos', { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() => {
		console.log('connection successful')
	})
	.catch((err) => {
		console.log(err)
	})

//router
app.use('/todo', todoHandler);
app.use('/user', userHandler);

app.get('/', (req, res) => {
	res.status(200).json({
		qstatus: 'successful',
		rows: [],
		length: 1
	});
});

app.get('/:something', (req, res) => {
	res.status(500).json({
		qstatus: 'unsuccessful',
		error: req.params.something + ' route not found'
	});
});

//default error handler
const errorHandler = (err, req, res, next) => {
	if (res.headersSent) {
		return next(err);
	} else {
		res.status(500).json({ error: err });
	}
}

//at last it handled the error
app.use(errorHandler);
//server listen at
app.listen(PORT, () => {
	console.log(`Server listening at http://localhost:${PORT}`);
});