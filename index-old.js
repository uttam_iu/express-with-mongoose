const express = require('express');
const smartphoneHandler = require('./routerHandler/smartphoneHandler');
const app = express();
const PORT = 3000;
// const productRoute = express.Router();


const rows = [
	{ id: 1, title: 'samsung a1', category: 'smartphone', subcategory: 'samsung', availability: true, price: 10000 },
	{ id: 2, title: 'samsung a2', category: 'smartphone', subcategory: 'samsung', availability: true, price: 5999 },
	{ id: 3, title: 'samsung a3', category: 'smartphone', subcategory: 'samsung', availability: true, price: 20599 },
	{ id: 4, title: 'samsung a4', category: 'smartphone', subcategory: 'samsung', availability: true, price: 16799 },
	{ id: 5, title: 'samsung a5', category: 'smartphone', subcategory: 'samsung', availability: false, price: 60999 },
	{ id: 6, title: 'samsung a6', category: 'smartphone', subcategory: 'samsung', availability: true, price: 42999 },
	{ id: 7, title: 'samsung a7', category: 'smartphone', subcategory: 'samsung', availability: true, price: 79999 },
	{ id: 8, title: 'nokia 1', category: 'smartphone', subcategory: 'nokia', availability: true, price: 15999 },
	{ id: 9, title: 'nokia 2', category: 'smartphone', subcategory: 'nokia', availability: true, price: 13999 },
	{ id: 10, title: 'nokia 3', category: 'smartphone', subcategory: 'nokia', availability: false, price: 8999 },
	{ id: 11, title: 'nokia 4', category: 'smartphone', subcategory: 'nokia', availability: true, price: 12599 },
	{ id: 12, title: 'nokia 5', category: 'smartphone', subcategory: 'nokia', availability: true, price: 22999 },
	{ id: 13, title: 'nokia 6', category: 'smartphone', subcategory: 'nokia', availability: false, price: 44999 },
	{ id: 14, title: 'nokia 7', category: 'smartphone', subcategory: 'nokia', availability: true, price: 39599 }
]


app.use(express.json());

// app.get('/', (req, res) => {
// 	let obj = {
// 		qstatus: 'successful',
// 		rows: rows,
// 		totalRows: rows.length
// 	}
// 	res.send(obj);
// });

app.post('/', (req, res) => {
	let obj = {
		qstatus: 'successful',
		rows: rows,
		totalRows: rows.length
	}
	res.send(obj);
});

// app.get('/smartphone', (req, res) => {
// 	let fRows = rows.filter(ech => { return (ech.category === 'smartphone') });
// 	let obj = {
// 		qstatus: 'successful',
// 		rows: fRows,
// 		totalRows: fRows.length
// 	}
// 	res.send(obj);
// });

app.post('/:category', (req, res) => {
	// console.log(req.params.category)
	sendResp(req, res);
});

app.post('/:category/:subcategory', (req, res) => {
	sendResp(req, res);
});

function sendResp(req, res) {
	const minPrice = req.body.minPrice || 0;
	const maxPrice = req.body.maxPrice || 99999999999999999999999999;
	const sCategory = req.params.subcategory || null;
	const category = req.params.category || null;
	
	const fRows = rows.filter(ech => {
		return (ech.category === category
			&& ech.subcategory === sCategory
			&& ech.price >= minPrice && ech.price <= maxPrice
		);
	});
	const obj = {
		qstatus: 'successful',
		rows: fRows,
		totalRows: fRows.length
	}

	res.send(obj);
}

// app.use(productRoute)
// productRoute.post('/smartphone', smartphoneHandler);


app.put('/', (req, res) => {
	res.send('Hello World! consoled by PUT request');
});

app.delete('/', (req, res) => {
	res.send('Hello World! consoled by DELETE request');
});


app.listen(PORT, () => {
	console.log(`Server listening at http://localhost:${PORT}`);
});